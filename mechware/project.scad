module rod(dia=8, len=300){
    cylinder(r=dia/2, h=len, center=true);
}

module nut(hex_dia=15, int_dia=8, len=6){
    difference(){
        cylinder(r=hex_dia/2, h=len, $fn=6, center=true);
        cylinder(r=int_dia/2, h=len+$fix*2, center=true);
    }
}

module motor(
    width=42.3,
    height=50,
    
    ring_dia=22,
    ring_len=2,
    
    mount_dia=3,
    mount_len=8,
    mount_head=6,
    mount_offset=31/2,
    
    clearance=0.1,
){
    outer_width=width+clearance*2;

    union(){
        translate([-outer_width/2, -outer_width/2, -height])
        cube([outer_width, outer_width, height+clearance]);
        
        cylinder(r=ring_dia/2+clearance, h=ring_len);

        for(m=[0, 1], n=[0, 1])
        mirror([0, m, 0])
        mirror([n, 0, 0])
        translate([mount_offset, mount_offset, 0]){
            cylinder(r=mount_dia/2+clearance, h=mount_len*2);
            
            translate([0, 0, mount_len])
            cylinder(r=mount_head/2+clearance, h=mount_len);
        }
    }
}

module plate(
    width=300,
    length=300,
    thickness=7,
){
    translate([-width/2, -length/2, 0])
    cube([width, length, thickness]);
}

module axis_holder(
    axis_dia=8,
    axis_height=12,
    
    mount_dia=8,
    mount_offset=20,
    
    thickness=6,
    
    clearance=0.2
){
    width=(mount_offset+mount_dia/2+thickness)*2;
    height=axis_height+axis_dia/2+thickness;
    length=mount_dia+thickness*2;
    mount=width-thickness*4-mount_dia*2;
    
    difference(){
        union(){
            translate([-length/2, -width/2, 0])
            cube([length, width, height-(axis_dia/2+thickness)]);
            
            translate([-length/2, 0, axis_height])
            rotate([0, 90, 0])
            cylinder(r=axis_dia/2+thickness, h=length);
        }
        
        translate([-length/2-$fix, 0, axis_height]){
            rotate([0, 90, 0])
            cylinder(r=axis_dia/2+clearance, h=length+$fix*2);
        }
        
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([0, mount_offset, -$fix])
        cylinder(r=mount_dia/2+clearance, h=axis_height+$fix*2);
    }
}

module coil_holder(
    axis_dia=16,
    axis_height=30,
    
    bearing_dia=22,
    bearing_offset=12,
    
    mount_dia=8,
    mount_offset=30,
    
    thickness=6,
    
    clearance=0.2,
    
    type="motor"/*bearing*/
){
    width=(mount_offset+mount_dia/2+thickness)*2;
    height=axis_height+axis_dia/2+thickness;
    length=mount_dia+thickness*2;
    mount=width-thickness*4-mount_dia*2;
    
    difference(){
        union(){
            translate([-length/2, -width/2, 0])
            cube([length, width, height-(axis_dia/2+thickness)]);

            if(type=="bearing"){
                translate([-length/2, 0, axis_height])
                rotate([0, 90, 0])
                cylinder(r=bearing_dia/2+thickness, h=length);
            }
            
            if(type=="motor"){
                translate([-length/2, -mount/2, axis_height])
                cube([length, mount, mount/2]);
            }
        }
        
        translate([-length/2-$fix, 0, axis_height]){
            rotate([0, 90, 0])
            cylinder(r=axis_dia/2+clearance, h=length+$fix*2);
            
            if(type=="bearing"){
                translate([length-bearing_offset, 0, 0])
                rotate([0, 90, 0])
                cylinder(r=bearing_dia/2+clearance, h=length+$fix*2);
            }
        }
        
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([0, mount_offset, -$fix])
        cylinder(r=mount_dia/2+clearance, h=axis_height+$fix*2);
        
        if(type=="motor"){
            translate([0, 0, axis_height])
            rotate([0, 90, 0]){
                motor();
                stepper_motor_mount(17);
            }
        }
    }
}

module carriage_holder(
    axis_dia=16,
    axis_height=30,
    rod_offset=20,
    
    bearing_dia=22,
    bearing_offset=12,
    
    mount_dia=8,
    mount_offset=30,
    
    thickness=6,
    
    clearance=0.2,
    
    type="motor"/*bearing*/
){
    width=(mount_offset+mount_dia/2+thickness)*2;
    height=axis_height+axis_dia/2+thickness;
    length=mount_dia+thickness*2;
    mount=width-thickness*4-mount_dia*2;

    translate([0, rod_offset, 0])
    difference(){
        union(){
            translate([-length/2, -width/2-(rod_offset+mount_dia/2+thickness), 0])
            cube([length, width+(rod_offset+mount_dia/2+thickness), height-(axis_dia/2+thickness)]);
            
            if(type=="bearing"){
                translate([-length/2, 0, axis_height])
                rotate([0, 90, 0])
                cylinder(r=bearing_dia/2+thickness, h=length);
            }
            
            if(type=="motor"){
                translate([-length/2, -mount/2, axis_height])
                cube([length, mount, mount/2]);
            }

            translate([-length/2, -rod_offset*2, axis_height])
            rotate([0, 90, 0])
            cylinder(r=mount_dia/2+thickness, h=length);
        }
        
        translate([-length/2-$fix, 0, axis_height]){
            rotate([0, 90, 0])
            cylinder(r=axis_dia/2+clearance, h=length+$fix*2);
            
            if(type=="bearing"){
                translate([length-bearing_offset, 0, 0])
                rotate([0, 90, 0])
                cylinder(r=bearing_dia/2+clearance, h=length+$fix*2);
            }
        }
        
        for(m=[mount_offset, -mount_offset-(rod_offset+mount_dia/2+thickness)])
        translate([0, m, -$fix])
        cylinder(r=mount_dia/2+clearance, h=axis_height+$fix*2);
        
        if(type=="motor"){
            translate([0, 0, axis_height])
            rotate([0, 90, 0]){
                motor();
                stepper_motor_mount(17);
            }
        }

        translate([-length/2-$fix, -rod_offset*2, axis_height])
        rotate([0, 90, 0])
        cylinder(r=mount_dia/2, h=length+$fix*2);
    }
}

module carriage(
    width=30,
    thickness=6,
    
    rod_dist=40,
    rod_dia=8,

    mnt_dist=40,
    mnt_dia=8,
    mnt_offset=20,
    
    clearance=0.2,
){
    height=rod_dia+thickness*2;
    
    difference(){
        minkowski(){
            translate([-width/2, -rod_dist/2, 0])
            cube([width-$fix, rod_dist+mnt_offset+mnt_dia, $fix]);
            
            rotate([0, 90, 0])
            cylinder(r=height/2, h=$fix);
        }
        
        for(m=[0, 1])
        mirror([0, m, 0])
        rotate([0, 90, 0])
        translate([0, rod_dist/2, -width/2-$fix])
        cylinder(r=rod_dia/2+clearance, h=width+$fix*2);
        
        translate([0, rod_dist/2, 0]){
            rotate([0, 90, 0])
            nut(hex_dia=15+clearance, int_dia=rod_dia-$fix, len=6+clearance*2);
            
            translate([-(3+clearance), -(15/2+clearance)*sqrt(3)/2, -(thickness+rod_dia/2+$fix)])
            cube([6+clearance*2, (15/2+clearance)*sqrt(3), thickness+rod_dia/2+$fix]);
        }
        
        translate([0, mnt_offset, 0])
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([0, mnt_dist/2, -height/2-$fix]){
            cylinder(r=mnt_dia/2+clearance, h=height+$fix*3);
            
            translate([0, 0, 3])
            nut(hex_dia=15+clearance, int_dia=rod_dia-$fix);
            
            translate([0, 0, 14])
            cylinder(r=mnt_dia, h=6+clearance+$fix*2);
        }        
    }
}

module guide(
    width=30,
    length=120,
    height=10,
    offset=0,
    
    mnt_dist=80,
    mnt_dia=8,
    mnt_offset=20,
    
    clearance=0.2,
){
    difference(){
        translate([-width/2, -length/2, 0])
        cube([width, length, height]);
        
        translate([0, -mnt_dist/2+mnt_dia/2, -$fix])
        minkowski(){
            translate([0, -mnt_dia/2, 0])
            cube([$fix, mnt_dist+clearance, height]);
            
            cylinder(r=mnt_dia/2+clearance, h=$fix*2);
        }
    }
}

use <../extern/MCAD/involute_gears.scad>
use <../extern/MCAD/motors.scad>
use <../extern/MCAD/bearing.scad>

module design(){
    *translate([0, 0, 13])
    plate();

    translate([0, -200, 50]){
        translate([-140, 0, 0])
        axis_holder();

        translate([140, 0, 0])
        axis_holder();
    }
    
    *translate([-150, 20, 50])
    rotate([0, 90, 0])
    stepper_motor_mount(17);
    
    translate([0, 0, 50]){
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([0, 20, 0])
        rotate([0, 90, 0])
        rod();
        
        translate([0, 20, 0])
        rotate([0, 90, 0])
        nut();
        
        translate([150-7, 20, 0])
        rotate([0, 90, 0])
        bearing();
        //gear(48, );
        
        translate([-140, 0, -30])
        carriage_holder(type="motor");
        
        translate([140, 0, -30])
        carriage_holder(type="bearing");
    }

    translate([0, 200, 50]){
        translate([-140, 0, 0])
        coil_holder(type="motor");
        
        *translate([-140-10, 0, 14])
        rotate([0, 90, 0])
        stepper_motor_mount(17);
        
        translate([140, 0, 0])
        coil_holder(type="bearing");
        
        translate([150-8, 0, 30])
        rotate([0, 90, 0])
        bearing();
    }
    
    translate([0, 0, 50])
    carriage();
    
    translate([0, 0, 60])
    guide();
}

module print(){
    translate([5, 0, 10])
    rotate([0, 90, 0])
    axis_holder();

    translate([-5, 0, 10])
    rotate([0, 90, 180])
    axis_holder();
}

$fix=0.01;
//$fix=1;
$fn=100;

difference(){
    design();

    *translate([0, -100, -100])
    cube([200, 200, 200]);
}

*print();
